package async

import kotlinx.coroutines.*
import kotlin.system.measureTimeMillis


import kotlinx.coroutines.*

/* fun main() {
    runBlocking {
        println("Weather forecast")
        val forecast: Deferred<String> = async {
            getForecast()
        }
        val temperature: Deferred<String> = async {
            getTemperature()
        }
        println("${forecast.await()} ${temperature.await()}")
        println("Have a good day!")
    }
}

suspend fun getForecast(): String {
    delay(1000)
    return "Sunny"
}

suspend fun getTemperature(): String {
    delay(1000)
    return "30\u00b0C"
}*/

fun main() {
    var weather: Deferred<String>
    var temperature: Deferred<String>

    val time = measureTimeMillis {

        runBlocking {
            println("Weather forecast")
            println(
                "forecast execution time: ${
                    measureTimeMillis {
                        weather = async {
                            getForecast()
                        }
                    }
                } ms"
            )

            println(
                "forecast retrieval time: ${
                    measureTimeMillis {
                        weather.await()
                    }
                } ms"
            )

            println(
                "temperature execution time: ${
                    measureTimeMillis {
                        temperature = async {
                            getTemperature()
                        }
                    }
                } ms"
            )
            println(
                "temperature retrieval time: ${
                    measureTimeMillis {
                        temperature.await()
                    }
                } ms"
            )

        }

    }
    println("Execution time: ${time} ms")
}

suspend fun getForecast(): String {
    delay(1000)
    return "Sunny"
}

suspend fun getTemperature(): String {
    delay(600)
    return "30\u00b0C"
}