package launch
import kotlinx.coroutines.*
import kotlin.system.measureTimeMillis

fun main() {
    val time = measureTimeMillis {

       runBlocking {
            println("Weather forecast")
            println(
                "forecast execution time: ${
                    measureTimeMillis {
                        launch {
                            printForecast()
                        }
                    }
                } ms"
            )

            println(
                "temperature execution time: ${
                    measureTimeMillis {
                        launch {
                            printTemperature()
                        }
                    }
                } ms"
            )


        }

    }
    println("Execution time: ${time} ms")
}

suspend fun printForecast() {
    delay(1000)
    println("Sunny")
}

suspend fun printTemperature() {
    delay(600)
    println("30\u00b0C")
}