package suspendMain.weaterReport

import kotlinx.coroutines.delay

suspend fun main() {
        println("Weather forecast")
        printForecast()
        printTemperature()
}

suspend fun printForecast() {
    delay(1000)
    println("Sunny")
}

suspend fun printTemperature() {
    delay(1000)
    println("30\u00b0C")
}