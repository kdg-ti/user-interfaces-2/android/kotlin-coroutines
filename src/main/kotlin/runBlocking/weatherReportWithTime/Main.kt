package runBlocking.weatherReportWithTime

import kotlin.system.measureTimeMillis
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking

fun main() {
    val time = measureTimeMillis {
        runBlocking {
            println("Weather forecast")
            println("forecast execution time: ${measureTimeMillis{printForecast()}} ms")
            println("temperature execution time: ${measureTimeMillis{printTemperature()}} ms")
        }
    }
    println("Execution time: ${time} ms")
}
suspend fun printForecast() {
    delay(1000)
    println("Sunny")
}

suspend fun printTemperature() {
    delay(400)
    println("30\u00b0C")
}